package models

type SearchRequest struct {
	Query string `json:"query"`
}

type GeoResponse struct {
	Addresses []*Address `json:"suggestions"`
}

type Address struct {
	Value string `json:"value"`
}
