package modules

import (
	"gitlab/cachegeo/internal/db/adapter"
	gstorage "gitlab/cachegeo/internal/modules/geo/storage"
)

type Storages struct {
	Geo gstorage.GeoStorager
}

func NewStorages(sqlAdapter *adapter.SQLAdapter) *Storages {
	return &Storages{
		Geo: gstorage.NewGeoStorage(sqlAdapter),
	}
}
