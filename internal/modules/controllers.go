package modules

import (
	"gitlab/cachegeo/internal/infrastructure/component"
	gcontroller "gitlab/cachegeo/internal/modules/geo/controller"
)

type Controllers struct {
	Geo gcontroller.GeoController
}

func NewControllers(services *Services, components *component.Components) *Controllers {

	return &Controllers{
		Geo: gcontroller.NewGeoCtl(services.GeoService, components),
	}
}
