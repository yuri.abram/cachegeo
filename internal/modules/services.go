package modules

import (
	"gitlab/cachegeo/internal/infrastructure/component"
	gserv "gitlab/cachegeo/internal/modules/geo/service"
)

type Services struct {
	GeoService gserv.GeoServicer
}

func NewServices(storage *Storages, components *component.Components) *Services {
	return &Services{
		GeoService: gserv.NewGeoService(components.Logger, storage.Geo),
	}
}
