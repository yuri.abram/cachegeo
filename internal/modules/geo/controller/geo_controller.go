package controller

import (
	"encoding/json"
	"gitlab/cachegeo/internal/infrastructure/component"
	"gitlab/cachegeo/internal/infrastructure/responder"
	"gitlab/cachegeo/internal/models"
	"gitlab/cachegeo/internal/modules/geo/service"
	"net/http"
)

type GeoController interface {
	SearchResponding(w http.ResponseWriter, r *http.Request)
}

type Geo struct {
	responder responder.Responder
	service   service.GeoServicer
}

func NewGeoCtl(service service.GeoServicer, components *component.Components) *Geo {
	return &Geo{responder: components.Responder, service: service}
}

// SearchResponding
// @Summary 		Поиск адреса
// @Description Поиск адреса./
// @ID 			search
// @Tags 		geocode
// @Accept 		json
// @Produce 	json
// @Param 		request body 		models.SearchRequest true "Search request"
// @Success 	200 	{object} 	models.GeoResponse
// @Failure 	400		{string}	string "Bad request"
// @Failure 	500		{string}	string "Internal server error"
// @Router		/api/address/search [post]
func (g *Geo) SearchResponding(w http.ResponseWriter, r *http.Request) {

	var sr models.SearchRequest
	err := json.NewDecoder(r.Body).Decode(&sr)
	if err != nil {
		g.responder.ErrorBadRequest(w, err)
		return
	}
	query := sr.Query
	out, err := g.service.Search(r.Context(), query)
	if err != nil {
		g.responder.ErrorBadRequest(w, err)
	}
	g.responder.OutputJSON(w, out)
}
