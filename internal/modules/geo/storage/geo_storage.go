package storage

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"gitlab/cachegeo/internal/db/adapter"
	"gitlab/cachegeo/internal/models"
	"net/http"
)

const (
	geolocateURL = "http://suggestions.dadata.ru/suggestions/api/4_1/rs/geolocate/address"
	suggestURL   = "http://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address"
	token        = "Token 08f266e9aceeebd38c7a744dacb7b9c9923c5264"
)

type GeoStorager interface {
	Search(ctx context.Context, query string) (models.GeoResponse, error)
}

type GeoStorage struct {
	adapter *adapter.SQLAdapter
}

func NewGeoStorage(sqlAdapter *adapter.SQLAdapter) *GeoStorage {
	return &GeoStorage{
		adapter: sqlAdapter,
	}
}

func (g *GeoStorage) Search(ctx context.Context, query string) (models.GeoResponse, error) {

	var res models.GeoResponse
	// поиск в кеше
	err := g.adapter.ListLevDist70(ctx, &res, "search_history", query)
	if err == nil && len(res.Addresses) > 0 {
		return res, nil
	}

	resp, err := DoRequest(&models.SearchRequest{Query: query})
	if err != nil {
		return res, err
	}
	defer resp.Body.Close()
	err = json.NewDecoder(resp.Body).Decode(&res)
	if err != nil {
		return res, err
	}

	// GeoResponse в AddressData
	addr := convertData(res)

	// запись в кеш таблицу
	if len(res.Addresses) > 0 {
		err = g.cacheResult(ctx, query, &addr)
		if err != nil {
			return res, err
		}
	}

	return res, nil

}

// kinda obsolete, too lazy to refactor
func DoRequest(reqType interface{}) (*http.Response, error) {

	apiURL, jsonBody, err := apiSelect(reqType)
	if err != nil {
		return nil, fmt.Errorf("bad request: %w", err)
	}

	req, err := http.NewRequest(
		"POST",
		apiURL,
		bytes.NewBuffer(jsonBody))

	if err != nil {
		return nil, fmt.Errorf("error creating request: %w", err)
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", token)

	client := http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error executing request: %w", err)
	}
	return resp, nil
}

func apiSelect(reqType interface{}) (apiURL string, jsonBody []byte, err error) {
	switch reqType.(type) {
	case *models.SearchRequest:
		apiURL = suggestURL
		jsonBody, err = json.Marshal(reqType)
		if err != nil {
			return "", nil, fmt.Errorf("bad request, error marshalling request body: %w", err)
		}
		return apiURL, jsonBody, nil

	default:
		return "", nil, fmt.Errorf("bad request")
	}
}

func (g *GeoStorage) cacheResult(ctx context.Context, query string, res *models.AddressData) error {

	queryId, _ := g.adapter.CreateWithID(ctx, &models.SearchHistory{
		Query: query,
	})

	addressId, _ := g.adapter.CreateWithID(ctx, res)

	err := g.adapter.Create(ctx, &models.SearchAddressLink{
		QueryID:     queryId,
		AddressesID: addressId,
	})

	if err != nil {
		return err
	}

	return nil
}

func convertData(geoResponse models.GeoResponse) models.AddressData {
	addresses := make([]string, len(geoResponse.Addresses))
	for i, address := range geoResponse.Addresses {
		addresses[i] = address.Value
	}
	return models.AddressData{
		Addresses: addresses,
	}
}
