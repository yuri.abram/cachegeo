package router

import (
	"github.com/go-chi/chi"
	httpSwagger "github.com/swaggo/http-swagger"
	_ "gitlab/cachegeo/docs"
	"gitlab/cachegeo/internal/infrastructure/component"
	"gitlab/cachegeo/internal/modules"
	"net/http"
)

func NewApiRouter(controllers *modules.Controllers, components *component.Components) http.Handler {
	r := chi.NewRouter()
	geo := controllers.Geo

	r.Get("/swagger/*", httpSwagger.Handler(
		httpSwagger.URL("http://localhost:8080/swagger/doc.json"),
	))

	r.Group(func(r chi.Router) {
		r.Route("/api/address", func(r chi.Router) {

			r.Post("/search", geo.SearchResponding)

		})
	})

	return r
}
