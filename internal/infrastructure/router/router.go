package router

import (
	"github.com/go-chi/chi/v5"
	chimw "github.com/go-chi/chi/v5/middleware"
	httpSwagger "github.com/swaggo/http-swagger"
	_ "gitlab/cachegeo/docs"
	"gitlab/cachegeo/internal/infrastructure/component"
	"gitlab/cachegeo/internal/infrastructure/middleware"
	"gitlab/cachegeo/internal/modules"
	"gitlab/cachegeo/internal/router"
	"net/http"
)

func NewRouter(controllers *modules.Controllers, components *component.Components) *chi.Mux {
	r := chi.NewRouter()
	setBasicMiddlewares(r)
	//setDefaultRoutes(r)

	r.Mount("/", router.NewApiRouter(controllers, components))
	return r
}

func setBasicMiddlewares(r *chi.Mux) {
	proxy := middleware.NewReverseProxy()
	r.Use(chimw.Recoverer)
	r.Use(proxy.ReverseProxy)
	r.Use(chimw.Logger)

	r.Use(chimw.RealIP)
	r.Use(chimw.RequestID)
}

func setDefaultRoutes(r *chi.Mux) {
	r.Get("/swagger/*", httpSwagger.Handler(httpSwagger.URL("http://localhost:8080/swagger/doc.json")))
	r.Get("/static/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))).ServeHTTP(w, r)
	})
}
