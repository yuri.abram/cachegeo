package component

import (
	"github.com/ptflp/godecoder"
	"gitlab/cachegeo/config"
	"gitlab/cachegeo/internal/infrastructure/responder"
	"go.uber.org/zap"
)

type Components struct {
	Conf      config.AppConf
	Responder responder.Responder
	Logger    *zap.Logger
	Decoder   godecoder.Decoder
}

func NewComponents(conf config.AppConf, responder responder.Responder, logger *zap.Logger, decoder godecoder.Decoder) *Components {
	return &Components{Conf: conf, Responder: responder, Logger: logger, Decoder: decoder}
}
