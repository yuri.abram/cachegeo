package main

import (
	"github.com/joho/godotenv"
	"gitlab/cachegeo/config"
	_ "gitlab/cachegeo/docs"
	"gitlab/cachegeo/internal/infrastructure/logs"
	"gitlab/cachegeo/run"
	"os"
)

// @title задача Реализация кэширования истории поиска
// @version 2.0
// @description  Геосервис API с авторизацией
// @host localhost:8080
// @BasePath /

func main() {

	// Загружаем переменные окружения из файла .env
	err := godotenv.Load()
	// Создаем конфигурацию приложения
	conf := config.NewAppConf()
	// Создаем логгер
	logger := logs.NewLogger(conf, os.Stdout)
	if err != nil {
		logger.Fatal("error loading .env file")
	}
	// Инициализируем конфигурацию приложения с логгером
	conf.Init(logger)
	// Создаем инстанс приложения
	app := run.NewApp(conf, logger)

	exitCode := app.
		// Инициализируем приложение
		Bootstrap().
		// Запускаем приложение
		Run()
	os.Exit(exitCode)
}
